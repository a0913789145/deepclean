 #!/usr/env python3
import numpy as np
import h5py
import pandas as pd

import gwpy
from gwpy.timeseries import TimeSeries
from gwpy.detector import Channel

import lalsimulation as lalsim
import lal

import bilby
from bilby.core.prior import Uniform
from bilby.core.utils import check_directory_exists_and_if_not_mkdir, logger

from pycbc.waveform import get_fd_waveform, get_td_waveform
from pycbc.detector import Detector

import matplotlib.pyplot as plt


frame_duration = 43200
for frame in range(1,41):
    start_time = 1251331218+frame_duration*(frame-1)
    n_injections = 625
    sampling_frequency = 16384
    channel_name_H1 = "H1:GDS-CALIB_STRAIN"
    channel_name_L1 = "L1:GDS-CALIB_STRAIN"
    interferometers = ["H1","L1"]
    injection_file = '/home/licheng.yang/Deepclean_MDC/injection_set/injections_set_mix.csv'

    params = pd.read_csv(injection_file).iloc[n_injections*(frame-1): n_injections*frame]

    zero = np.zeros(frame_duration*sampling_frequency)
    strain_H1 = TimeSeries(zero, t0=start_time, sample_rate=sampling_frequency, name = channel_name_H1, unit="", channel = channel_name_H1)
    strain_L1 = TimeSeries(zero, t0=start_time, sample_rate=sampling_frequency, name = channel_name_L1, unit="", channel = channel_name_L1)

    for n in range(n_injections*(frame-1),n_injections*frame):
        if params["approximant"][n] == 'IMRPhenomPv2':
            hp, hc = get_td_waveform(approximant=params["approximant"][n],
                                     coa_phase = params["coa_phase"][n],
                                     dec = params["dec"][n],
                                     distance = params["distance"][n],
                                     f_lower = params["f_lower"][n],
                                     f_ref = params["f_ref"][n],
                                     inclination = params["inclination"][n],
                                     # lambda1 = params["lambda1"][n],
                                     # lambda2 = params["lambda2"][n],
                                     mass1 = params["mass1"][n], 
                                     mass2 = params["mass2"][n],
                                     polarization = params["polarization"][n],
                                     ra = params["ra"][n],
                                     spin1x = params["spin1x"][n],
                                     spin1y = params["spin1y"][n],
                                     spin1z = params["spin1z"][n],
                                     spin2x = params["spin2x"][n],
                                     spin2y = params["spin2y"][n],
                                     spin2z = params["spin2z"][n],
                                     delta_t = 1.0/sampling_frequency)
        else:
            hp, hc = get_td_waveform(approximant=params["approximant"][n],
                                     coa_phase = params["coa_phase"][n],
                                     dec = params["dec"][n],
                                     distance = params["distance"][n],
                                     f_lower = params["f_lower"][n],
                                     f_ref = params["f_ref"][n],
                                     inclination = params["inclination"][n],
                                     lambda1 = params["lambda1"][n],
                                     lambda2 = params["lambda2"][n],
                                     mass1 = params["mass1"][n], 
                                     mass2 = params["mass2"][n],
                                     polarization = params["polarization"][n],
                                     ra = params["ra"][n],
                                     spin1x = params["spin1x"][n],
                                     spin1y = params["spin1y"][n],
                                     spin1z = params["spin1z"][n],
                                     spin2x = params["spin2x"][n],
                                     spin2y = params["spin2y"][n],
                                     spin2z = params["spin2z"][n],
                                     delta_t = 1.0/sampling_frequency)


        hp.start_time = params["tc"][n] + hp.start_time
        hc.start_time = params["tc"][n] + hc.start_time



        det_H1 = Detector(interferometers[0])
        det_L1 = Detector(interferometers[1])

        signal_H1 = det_H1.project_wave(hp, hc,
                                        params["ra"][n], 
                                        params["dec"][n],
                                        params["polarization"][n])
        signal_L1 = det_L1.project_wave(hp, hc,
                                        params["ra"][n], 
                                        params["dec"][n],
                                        params["polarization"][n])

        signal_H1_ts = TimeSeries(signal_H1, t0=int(float(signal_H1.start_time)*sampling_frequency)/sampling_frequency , sample_rate=sampling_frequency, name = channel_name_H1, unit="", channel = channel_name_H1)
        signal_L1_ts = TimeSeries(signal_L1, t0=int(float(signal_L1.start_time)*sampling_frequency)/sampling_frequency , sample_rate=sampling_frequency, name = channel_name_L1, unit="", channel = channel_name_L1)

        # signal_h1_ts = signal_h1_ts.taper()

        strain_H1 = strain_H1.inject(signal_H1_ts)
        strain_L1 = strain_L1.inject(signal_L1_ts)


        logger.info(f"finish n = {n}")


    file_name_H1="mdc_test_inject_H1_{}-{}.gwf".format(start_time,frame_duration)
    file_name_L1="mdc_test_inject_L1_{}-{}.gwf".format(start_time,frame_duration)
    logger.info(f"Saving {file_name_H1}")
    logger.info(f"Saving {file_name_L1}")
    strain_H1.write(f"/home/licheng.yang/Deepclean_MDC/H1_waveform_mix/{file_name_H1}")
    strain_L1.write(f"/home/licheng.yang/Deepclean_MDC/L1_waveform_mix/{file_name_L1}")

