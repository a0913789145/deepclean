import os
import numpy as np
import glob
from gwpy.timeseries import TimeSeries
import matplotlib.pyplot as plt
import pandas as pd
import bilby
from gwpy.segments import DataQualityFlag
segments = DataQualityFlag.query('H1:DMT-ANALYSIS_READY:1', 1251331218, 1253059218)
n_segment = len(segments.active)

frame_length = 2000
for n in range(n_segment):
    seg_start = int(segments.active[n][0])
    seg_end = int(segments.active[n][1])
    duration = seg_end - seg_start
    frame_number = duration//frame_length
    
    sed_1 ="""sed 's$dummytime${}$g' train_base.sh >> train_H1_train_on_inj.sh""".format(seg_start)
    if duration<frame_length:
        sed_2 ="""sed -i -- 's$dummyduration${}$g' train_H1_train_on_inj.sh""".format(duration)    
    else: 
        sed_2 ="""sed -i -- 's$dummyduration${}$g' train_H1_train_on_inj.sh""".format(frame_length)
        
    sed_3 ="""sed -i -- 's$dummysegment${}$g' train_H1_train_on_inj.sh""".format(n)
    os.system(sed_1)
    os.system(sed_2)
    os.system(sed_3)