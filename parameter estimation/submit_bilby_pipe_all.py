import os
import numpy as np
import glob
from gwpy.timeseries import TimeSeries
import matplotlib.pyplot as plt
import pandas as pd
import bilby

def get_flso (m1, m2):
    Mpc=1.0293e14;
    Msun=4.92549095e-6;
    return (6.**(3./2.)*np.pi*(m1+m2)*Msun)**(-1.);

pd.set_option('display.float_format', lambda x: '%.10f' % x)

path_injection_csv = "/home/licheng.yang/Deepclean_MDC/injection_set/injections_set_mix.csv"
injections = pd.read_csv(path_injection_csv)
injections['f_lso'] = get_flso(injections['mass1'], injections['mass2'])
injections_20day = injections[:25000]
injections_20day_60Hz = injections_20day[(injections_20day['f_lso'] < 70) & (injections_20day['f_lso'] > 55)]
inj_tc_list = injections['tc'].tolist()

path_active_segment_csv = "/home/licheng.yang/Deepclean_MDC/bilby_pipe/MDC_20_days/event_in_active_both.csv"
active_segment = pd.read_csv(path_active_segment_csv)
event_active_both = active_segment['Unnamed: 0'].tolist()

    
def get_file_containing_injection (file_list, inj_time):
    for the_file in file_list:
        start = int(the_file.split(".")[0].split("-")[-2:][0])
        duration = int(the_file.split(".")[0].split("-")[-2:][1])
        end =  start + duration
        if (inj_time > start) & (inj_time < end):
            return the_file
    return None


# the list of all files in the day 1
H1_frames_day1 = []
L1_frames_day1 = []
start = 1251331218
end = 1251331218 + 86400*20
for i in range(start, end, 3600):
    H1_frames_day1.append(f'H-H1_INJ_DC-{i}-3600.gwf')
    L1_frames_day1.append(f'L-L1_INJ_DC-{i}-3600.gwf')



H1_frames = []
L1_frames = []
for inj_time in inj_tc_list:
    H1_frames.append(get_file_containing_injection (H1_frames_day1, inj_time))
    L1_frames.append(get_file_containing_injection (L1_frames_day1, inj_time))
    

num = active_segment['0'].tolist()
for i in event_active_both[:]:

    sed_1 ="""sed 's$dummyframeH1${}$g' config_base.ini > config_{}.ini""".format(H1_frames[num[i]],num[i])
    sed_2 ="""sed -i -- 's$dummyframeL1${}$g' config_{}.ini""".format(L1_frames[num[i]],num[i])

    sed_3 ="""sed -i -- 's$dummyIndex${}$g' config_{}.ini""".format(num[i],num[i])
    sed_4 ="""sed -i -- 's$dummytime${}$g' config_{}.ini""".format(inj_tc_list[num[i]],num[i])

    os.system(sed_1)
    os.system(sed_2)
    os.system(sed_3)
    os.system(sed_4)

    os.system("bilby_pipe config_{}.ini --submit".format(num[i]))