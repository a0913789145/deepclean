import os
import math
import numpy as np
import glob
from gwpy.timeseries import TimeSeries
import matplotlib.pyplot as plt
import pandas as pd
import bilby
from gwpy.segments import DataQualityFlag

start = 1251331218
# end = 1251331218 +86400 
end = 1253059218 #20 days
segments = DataQualityFlag.query('H1:DMT-ANALYSIS_READY:1', start, end)
n_segment = len(segments.active)

frame_length = 3600

for n in range(len(segments.active)):
    segment_start = math.ceil((segments.active[n][0]-start)/frame_length)
    if n == len(segments.active)-1:
        segment_end = int((end-start)/frame_length)
    else:
        segment_end = math.ceil((segments.active[n+1][0]-start)/frame_length)
    
    for m in range(segment_start,segment_end):
        clean_start = start + frame_length*m
        sed_1 ="""sed 's$dummytime${}$g' clean_base.sh >> clean_H1_INJ_train_on_inj.sh""".format(clean_start)
        sed_2 ="""sed -i -- 's$dummyduration${}$g' clean_H1_INJ_train_on_inj.sh""".format(frame_length)
        sed_3 ="""sed -i -- 's$dummysegment${}$g' clean_H1_INJ_train_on_inj.sh""".format(n)
        os.system(sed_1)
        os.system(sed_2)
        os.system(sed_3)