import os
#import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import glob
import pickle
import scipy.stats as ss
import time
import copy
import bilby
from gwpy.timeseries import TimeSeries
import matplotlib.pyplot as plt
from chainconsumer import ChainConsumer

import lalsimulation as lalsim
import lal
from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters
from bilby.gw.prior import BBHPriorDict
from bilby.gw.source import lal_binary_black_hole

from IPython.display import Image, display
import h5py
import corner
import scipy.stats
from scipy.stats import norm


def get_flso (m1, m2):
    Mpc=1.0293e14;
    Msun=4.92549095e-6;
    return (6.**(3./2.)*np.pi*(m1+m2)*Msun)**(-1.);

def get_psd_from_fdomain_noise (fd_strain, freq):
    delta_f = freq[1] - freq[0]
    psd = 2 * np.real(fd_strain*fd_strain.conj()) * delta_f
    return psd

pd.set_option('display.float_format', lambda x: '%.10f' % x)


def compute_CI_width_frac(result1,result2,key):
    posterior1 = result1.posterior[key]
    posterior2 = result2.posterior[key]
    
    left_1 = np.percentile(posterior1, 5)
    right_1 = np.percentile(posterior1, 95)
    
    left_2 = np.percentile(posterior2, 5)
    right_2 = np.percentile(posterior2, 95)
    
    interval_width_1 = right_1 - left_1
    interval_width_2 = right_2 - left_2
    
    ifac = interval_width_2/interval_width_1
    return ifac
    

path_json = "/home/licheng.yang/Deepclean_MDC/injection_set/injections_set_bilby.json"
path_csv = "/home/licheng.yang/Deepclean_MDC/injection_set/injections_set_bilby.csv"
injections = pd.read_csv(path_csv)

path_active_segment_csv = "/home/licheng.yang/Deepclean_MDC/bilby_pipe/MDC_20_days/event_in_active_both.csv"
active_segment = pd.read_csv(path_active_segment_csv)
event_active_both = active_segment['0'].tolist()

CI_width_frac = {}
CI_width_frac['Injction_number']= {}
CI_width_frac['CI_width_frac_mass_ratio'] = {}
CI_width_frac['CI_width_frac_chirp_mass'] = {}
CI_width_frac['CI_width_frac_a_1'] = {}
CI_width_frac['CI_width_frac_a_2'] = {}
CI_width_frac['CI_width_frac_tilt_1'] = {}
CI_width_frac['CI_width_frac_tilt_2'] = {}
CI_width_frac['CI_width_frac_phi_12'] = {}
CI_width_frac['CI_width_frac_phi_jl'] = {}
CI_width_frac['CI_width_frac_dec'] = {}
CI_width_frac['CI_width_frac_ra'] = {}
CI_width_frac['CI_width_frac_theta_jn'] = {}
CI_width_frac['CI_width_frac_psi'] = {}
CI_width_frac['CI_width_frac_luminosity_distance'] = {}
CI_width_frac['CI_width_frac_phase'] = {}
CI_width_frac['CI_width_frac_geocent_time'] = {}

confidence_org = []
confidence_dc = []
confidence_dc_train_on_inj = []

parameter_keys = ['mass_ratio', 'chirp_mass', 'a_1', 'a_2', 'tilt_1', 'tilt_2', 'phi_12', 'phi_jl', 'luminosity_distance', 'dec', 'ra', 'theta_jn', 'psi', 'phase', 'geocent_time']
run_list = event_active_both[:]

for inj_id in run_list[:]:
    try:
        injection_parameters = injections.iloc[inj_id].to_dict()
        
        truth = dict(mass_ratio=injection_parameters['mass_2']/injection_parameters['mass_1'],
             chirp_mass=bilby.gw.conversion.component_masses_to_chirp_mass(injection_parameters['mass_2'], injection_parameters['mass_1']),
             a_1=injection_parameters['a_1'],
             a_2=injection_parameters['a_2'],
             tilt_1=injection_parameters['tilt_1'],
             tilt_2=injection_parameters['tilt_2'],
             phi_12=injection_parameters['phi_12'],
             phi_jl=injection_parameters['phi_jl'],
             luminosity_distance=injection_parameters['luminosity_distance'],
             dec=injection_parameters['dec'],
             ra=injection_parameters['ra'],
             theta_jn=injection_parameters['theta_jn'],
             psi=injection_parameters['psi'],
             phase=injection_parameters['phase'],
             geocent_time=injection_parameters['geocent_time'])

        resultsfile_org = glob.glob(f"ORG/injection_{inj_id}/result/*.json")
        results_org = bilby.result.read_in_result(resultsfile_org[0])
        results_org.injection_parameters = truth

        resultsfile_dc = glob.glob(f"DC/injection_{inj_id}/result/*.json")
        results_dc = bilby.result.read_in_result(resultsfile_dc[0])
        results_dc.injection_parameters = truth
        
        resultsfile_dc_train_on_inj = glob.glob(f"DC_train_on_inj/injection_{inj_id}/result/*.json")
        results_dc_train_on_inj = bilby.result.read_in_result(resultsfile_dc_train_on_inj[0])
        results_dc_train_on_inj.injection_parameters = truth
        
    except:
        print(f"error at number {inj_id}")
        
    samples_org = results_org.samples
    samples_dc = results_dc.samples
    samples_dc_train_on_inj = results_dc_train_on_inj.samples
    labels = results_org.parameter_labels


    CI_width_frac['Injction_number'][inj_id] = inj_id
    CI_width_frac['CI_width_frac_mass_ratio'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[0])
    CI_width_frac['CI_width_frac_chirp_mass'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[1])
    CI_width_frac['CI_width_frac_a_1'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[2])
    CI_width_frac['CI_width_frac_a_2'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[3])
    CI_width_frac['CI_width_frac_tilt_1'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[4])
    CI_width_frac['CI_width_frac_tilt_2'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[5])
    CI_width_frac['CI_width_frac_phi_12'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[6])
    CI_width_frac['CI_width_frac_phi_jl'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[7])
    CI_width_frac['CI_width_frac_luminosity_distance'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[8])
    CI_width_frac['CI_width_frac_dec'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[9])
    CI_width_frac['CI_width_frac_ra'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[10])
    CI_width_frac['CI_width_frac_theta_jn'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[11])
    CI_width_frac['CI_width_frac_psi'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[12])
    CI_width_frac['CI_width_frac_phase'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[13])
    CI_width_frac['CI_width_frac_geocent_time'][inj_id] = compute_CI_width_frac(results_org, results_dc, parameter_keys[14])
    
    confidence_row_org = []
    confidence_row_dc = []
    confidence_row_dc_train_on_inj = []
    
    for param in parameter_keys:
        fx, bins = np.histogram(results_org.posterior[param],bins=40,density=True)
        x = []
        for k in range(len(bins)-1):
            x.append((bins[k]+bins[k+1])/2)
        
        sfx = fx.copy()
        sfx.sort()
        sfx[:] = sfx[::-1]
        dx = x[1]-x[0]
        nearest = np.argmin(np.absolute(np.array(x)-results_org.injection_parameters[param]))
        height_inj = fx[nearest]
        inds_sfx = np.where(sfx>height_inj)[0]
        prob = prob = np.sum(sfx[inds_sfx])*dx
        confidence_row_org.append(prob)
    confidence_org.append(confidence_row_org)
    
    for param in parameter_keys:
        fx, bins = np.histogram(results_dc.posterior[param],bins=40,density=True)
        x = []
        for k in range(len(bins)-1):
            x.append((bins[k]+bins[k+1])/2)
        
        sfx = fx.copy()
        sfx.sort()
        sfx[:] = sfx[::-1]
        dx = x[1]-x[0]
        nearest = np.argmin(np.absolute(np.array(x)-results_dc.injection_parameters[param]))
        height_inj = fx[nearest]
        inds_sfx = np.where(sfx>height_inj)[0]
        prob = prob = np.sum(sfx[inds_sfx])*dx
        confidence_row_dc.append(prob)
    confidence_dc.append(confidence_row_dc)
    
    for param in parameter_keys:
        fx, bins = np.histogram(results_dc_train_on_inj.posterior[param],bins=40,density=True)
        x = []
        for k in range(len(bins)-1):
            x.append((bins[k]+bins[k+1])/2)
        
        sfx = fx.copy()
        sfx.sort()
        sfx[:] = sfx[::-1]
        dx = x[1]-x[0]
        nearest = np.argmin(np.absolute(np.array(x)-results_dc_train_on_inj.injection_parameters[param]))
        height_inj = fx[nearest]
        inds_sfx = np.where(sfx>height_inj)[0]
        prob = prob = np.sum(sfx[inds_sfx])*dx
        confidence_row_dc_train_on_inj.append(prob)
    confidence_dc_train_on_inj.append(confidence_row_dc_train_on_inj)
    
CI_width_frac = pd.DataFrame.from_dict(CI_width_frac)
CI_width_frac.to_csv("CI_width_frac.csv",index=False)


fig = plt.figure(figsize=(25,15))
for i in range(15):
    ax = fig.add_subplot(3,5,i+1)
    ax.hist(CI_width_frac['CI_width_frac_' + parameter_keys[i]], bins = 20, label = "CI_width deepclean/orginal")
    ax.legend(loc = 'upper right')
    ax.set_title(parameter_keys[i])
plt.savefig('/home/licheng.yang/Deepclean_MDC/bilby_pipe/good_C00/CI_width_frac.png')


number_org = []
number_dc = []
percents = np.linspace(0, 1, 101)

for row in np.array(confidence_org).T:
    number_row = []
    for i in percents:
        num = 0
        for val in row:
            if val < i:
                num+=1
        number_row.append(float(num)/len(row))
    number_org.append(number_row)


for row in np.array(confidence_dc).T:
    number_row = []
    for i in percents:
        num = 0
        for val in row:
            if val < i:
                num+=1
        number_row.append(float(num)/len(row))
    number_dc.append(number_row)
    
    
fig = plt.figure(figsize=(25,15))

for i in range(15):
    ax = fig.add_subplot(3,5,i+1)
    ax.plot(percents, number_org[i],label = "orginal")
    ax.plot(percents, number_dc[i],label = "deepclean")
    ax.legend(loc = 'upper left')
    ax.plot(percents, percents, ls=':', color='#000000')
    ax.set_ylim(ymin=0, ymax=1)
    ax.set_xlim(xmin=0, xmax=1)
    ax.set_title(parameter_keys[i])
plt.savefig('/home/licheng.yang/Deepclean_MDC/bilby_pipe/good_C00/pp_plot.png')